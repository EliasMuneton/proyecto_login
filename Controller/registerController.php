<?php
 
include('../Model/config.php');
session_start();
 
if (isset($_POST["ACCION"])) {
    if ($_POST["ACCION"] == "REGISTER") {
 
        $name = $_POST['NAME'];
        $firstname = $_POST['FIRSTNAME'];
        $lastname = $_POST['LASTNAME'];
        $username = $_POST['USER'];
        $pass1 = $_POST['PASS'];
        
        $pass = password_hash($pass1, PASSWORD_BCRYPT);
    
        $query = $connection->prepare("SELECT * FROM users WHERE username=:username");
        $query->bindParam("username", $username, PDO::PARAM_STR);   
        $query->execute();
        if ($query->rowCount() > 0) {
            $arr = array("STATUS" => "ERROR", "MESSAGE" => "EL NOMBRE DE USUARIO YA EXISTE");
            echo json_encode($arr);
        }
    
        if ($query->rowCount() == 0) {
            $query = $connection->prepare("INSERT INTO proyect_login.users (username, pass, name, firstname, lastname) VALUES (:username,:pass,:name,:firstname,:lastname)");
            $query->bindParam("username", $username, PDO::PARAM_STR);
            $query->bindParam("pass", $pass, PDO::PARAM_STR);
            $query->bindParam("name", $name, PDO::PARAM_STR);
            $query->bindParam("firstname", $firstname, PDO::PARAM_STR);
            $query->bindParam("lastname", $lastname, PDO::PARAM_STR);
            
            
            $result = $query->execute();
    
            if ($result) {
                $arr = array("STATUS" => "OK");
                echo json_encode($arr);
            } else {
                $arr = array("STATUS" => "ERROR", "MESSAGE" => "OCURRIÓ UN PROBLEMA");
                echo json_encode($arr);
            }
        }
    }
}
 
?>