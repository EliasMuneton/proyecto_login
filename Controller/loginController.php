<?php
 
 include('../Model/config.php');
 session_start();
 
if (isset($_POST["ACCION"])) {
    if ($_POST["ACCION"] == "LOGIN") {
        $username = $_POST['USER'];
        $password = $_POST['PASS'];
    
        $query = $connection->prepare("SELECT * FROM users WHERE username=:username");
        $query->bindParam("username", $username, PDO::PARAM_STR);   
        $query->execute();
        $result = $query->fetch();

        if (!$result) {
            $arr = array("STATUS" => "ERROR", "MESSAGE" => "OCURRIÓ UN PROBLEMA");
            echo json_encode($arr);
        } else {
            if (password_verify($password, $result['pass'])) {
                $_SESSION['USER_ID'] = $result['id'];
                $arr = array("STATUS" => "OK");
                echo json_encode($arr);
            } else {
                $arr = array("STATUS" => "ERROR", "MESSAGE" => "LAS CREDENCIALES NO SON CORRECTAS");
                echo json_encode($arr);
            }
        }
    }

    if ($_POST["ACCION"] == "NEWPASS") {

        $id = $_POST['USER_ID'];
        $newpass1 = $_POST['PASS'];
        $lastpass1 = $_POST['LASTPASS'];
        
        $pass = password_hash($newpass1, PASSWORD_BCRYPT);

        $query = $connection->prepare("SELECT * FROM users WHERE id=:id");
        $query->bindParam("id", $id, PDO::PARAM_INT);
        $query->execute();


        if ($query->rowCount() > 0) {
            $result = $query->fetch();

            
            if (password_verify($lastpass1, $result['pass'])) {

                $query = $connection->prepare("UPDATE users SET pass=:pass where id = :id");
                $query->bindParam("pass", $pass, PDO::PARAM_STR);
                $query->bindParam("id", $id, PDO::PARAM_INT);
        
                $result = $query->execute();
                if ($result) {
                    $arr = array("STATUS" => "OK");
                    echo json_encode($arr);
                } else {
                    $arr = array("STATUS" => "ERROR", "MESSAGE" => "OCURRIÓ UN PROBLEMA");
                    echo json_encode($arr);
                }
            } else {
                $arr = array("STATUS" => "ERROR", "MESSAGE" => "LA CONTRASEÑA ACTUAL NO ES CORRECTA");
                echo json_encode($arr);
            }
        } else {
            $arr = array("STATUS" => "ERROR", "MESSAGE" => "NO EXISTE EL USUARIO");
            echo json_encode($arr);
        }
    }

    if ($_POST["ACCION"] == "LOGOUT") {
        unset($_SESSION['USER_ID']);
        session_destroy();
        $arr = array("STATUS" => "OK");
        echo json_encode($arr);
    }

}
 
?>