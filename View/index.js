
$( window ).on( "load", function() {
    
    
    $('#btn-login').click(function(){
        login();
    });
});

async function peticion_ws(metodo, path, body) {
    let response = await fetch(`../Controller/loginController.php`, {
        method: metodo,
        body: body
    }).catch(function (error) {
        $.error("Ocurri&oacute; un error intentando realizar la operaci&oacute;n");
        return;
    });
    let result = await response.json();
    return result;
}


async function login() {
    let form = $('#register-form').parsley();
    form.validate();
    if (form.isValid()) {
        var formElement = document.getElementById("register-form");
        var formData = new FormData(formElement);
        const result = await peticion_ws('POST', '',formData);
        console.log(result);
        if (result.STATUS == "OK") {
            window.location.href = 'Pass/changePassword.php';
        } else {
            alert (result.MESSAGE);
        }
    } 
}