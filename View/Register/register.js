
$( window ).on( "load", function() {
    
    
    $('#btn-register').click(function(){
        register();
    });
});

async function peticion_ws(metodo, path, body) {
    let response = await fetch(`../../Controller/registerController.php`, {
        method: metodo,
        body: body
    }).catch(function (error) {
        $.error("Ocurri&oacute; un error intentando realizar la operaci&oacute;n");
        return;
    });
    let result = await response.text();
    return result;
}


async function register() {
    let form = $('#register-form').parsley();
    form.validate();
    if (form.isValid()) {
        var formElement = document.getElementById("register-form");
        var formData = new FormData(formElement);
        
        const result = await peticion_ws('POST', '',formData);
        console.log(result)
        var obj = JSON.parse(result);
        if(obj.STATUS == "OK") {
            alert("SE REGISTRO CORRECTAMNENTE EL USUARIO");
            clear();
            window.location.href = '../index.php';
        } else {
            alert(obj.MESSAGE);
        }

    }
}


let clear =  function () {
    $("#NAME").val("");
    $("#FIRSTNAME").val("");
    $("#LASTNAME").val("");
    $("#USER").val("");
    $("#PASS").val("");
}