
$( window ).on( "load", function() {
    
    
    $('#btn-update').click(function(){
        updatePass();
    });

    $('#btn-logout').click(function(){
        logout();
    });
});

async function peticion_ws(metodo, path, body) {
    let response = await fetch(`../../Controller/loginController.php`, {
        method: metodo,
        body: body
    }).catch(function (error) {
        $.error("Ocurri&oacute; un error intentando realizar la operaci&oacute;n");
        return;
    });
    let result = await response.text();
    return result;
}


async function updatePass() {
    
    let form = $('#register-form').parsley();
    form.validate();
    if (form.isValid()) {

        var formElement = document.getElementById("register-form");
        var formData = new FormData(formElement);
        
        const result = await peticion_ws('POST', '',formData);
        console.log(result)
        var obj = JSON.parse(result);
        if(obj.STATUS == "OK") {
            alert("SE ACTUALIZO CORRECTAMENTE EL PASSWORD");
            clear();
        } else {
            alert(obj.MESSAGE);
        }
    } 
}


let clear =  function () {
    $("#PASS").val("");
    $("#LASTPASS").val("");
}

async function logout() {

    var formData = new FormData();
    formData.append("ACCION", "LOGOUT");
    const result = await peticion_ws('POST', '',formData);
    console.log(result)
    var obj = JSON.parse(result);
    if(obj.STATUS == "OK") {
        window.location.href = '../index.php';
    } else {
        alert(obj.MESSAGE);
    }
}